from django.db import models
from blog.models.article import Article
from django.contrib.auth.models import User

# Create your models here.
class Comment(models.Model):
    name = models.CharField(max_length=55,help_text='กรุณากรอกชื่อ',error_messages={'required': 'กรุณากรอกชื่อ'})
    message = models.CharField(max_length=255,help_text='กรุณากรอกความคิดเห็น ไม่เกิน255ตัวอักษร',error_messages={'required': 'กรุณากรอกความคิดเห็น'})
    article = models.ForeignKey(Article, related_name='comments', on_delete=models.CASCADE,default=None,null=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE,default=None,null=True)
    created_datetime = models.DateTimeField(auto_now=True)
    is_enabled = models.BooleanField(default=True)

    class Meta:
            ordering = ["-created_datetime"]

    def __str__(self):

        return "%s - %s" % (self.article.title,self.message)