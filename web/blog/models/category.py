from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=55,unique=True)
    description = models.CharField(max_length=255)
    is_enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.name