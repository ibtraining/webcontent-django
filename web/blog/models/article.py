from django.db import models
from blog.models.category import Category
import datetime

# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=55,unique=True)
    image = models.ImageField(upload_to='blog/article/', max_length=900,default=None)
    category = models.ForeignKey(Category, on_delete=models.CASCADE,default=None,null=True)
    description = models.CharField(max_length=255,default=None,null=True)
    detail = models.TextField()
    created_datetime = models.DateTimeField(auto_now=True)
    updated_datetime = models.DateTimeField(default=None,null=True)
    is_enabled = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        self.updated_datetime = datetime.datetime.now()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title
    