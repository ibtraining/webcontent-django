from django.shortcuts import render
from blog.models.category import Category
from blog.models.article import Article
from blog.models.comment import Comment
from django.core.paginator import Paginator
from blog.forms.comment import CommentForm

# Create your views here.
def articleDetailView(request,pk):
    category_list = Category.objects.filter(is_enabled=True)
    article = Article.objects.get(pk=pk,is_enabled=True)
    
    form_comment = CommentForm()

    # Submit Comment
    if request.method == "POST":

        form_comment = CommentForm(request.POST)

        # Check validation 
        if form_comment.is_valid():
            comment = form_comment.save(commit=False)

            # Save comment
            if request.user.is_authenticated:
                comment.creator = request.user

            comment.article = article
            comment.save()

            # Reset CommentForm
            form_comment = CommentForm()

    # Context
    context = {
        'category_list' : category_list,
        'article' : article,
        'form_comment': form_comment
    }
    return render(request, 'blog/views/article/detail.html',context)