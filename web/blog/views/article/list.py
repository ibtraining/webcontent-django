from django.shortcuts import render
from blog.models.category import Category
from blog.models.article import Article
from django.core.paginator import Paginator
from blog.forms.comment import CommentForm

# Create your views here.

def articleListView(request):
    
    category_list = Category.objects.filter(is_enabled=True)
    article_list = Article.objects.order_by('-created_datetime').filter(is_enabled=True)

    # sort
    sort = request.GET.get('sort','asc')
    sort_label = 'น้อยไปมาก'
    if sort == 'desc':
        article_list = article_list.order_by('-id')
        sort_label = 'มากไปน้อย'
    else:
        article_list = article_list.order_by('id')


    # filter
    category = None
    category_id = request.GET.get('category_id',None)
    if category_id is not None:
        try:
            category = Category.objects.get(id=category_id)
            article_list = article_list.filter(category=category)
        except:
            pass

    # pagination
    paginator = Paginator(article_list, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    # Context
    context = {
        'category_list' : category_list,
        'article_list' : article_list,
        'page_obj' : page_obj,
        'category': category,
        'sort': sort,
        'sort_label': sort_label
    }
    return render(request, 'blog/views/article/list.html',context)