from django.shortcuts import render
from blog.models.category import Category
from blog.models.article import Article
from django.core.paginator import Paginator
from blog.forms.signin import SigninForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse

# Create your views here.
def logoutView(request):
    logout(request)

    response = redirect(reverse('blog-home'))
    return response