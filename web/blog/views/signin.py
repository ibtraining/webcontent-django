from django.shortcuts import render
from blog.models.category import Category
from blog.models.article import Article
from django.core.paginator import Paginator
from blog.forms.signin import SigninForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse

# Create your views here.
def signinView(request):
    category_list = Category.objects.filter(is_enabled=True)
    
    form_auth = SigninForm()
    # Submit Signin
    if request.method == "POST":

        form_auth = SigninForm(request.POST)

        # Check validation 
        if form_auth.is_valid():

            # Check Login
            username = form_auth.cleaned_data['username']
            password = form_auth.cleaned_data['password']
            is_remember_me = form_auth.cleaned_data['is_remember_me']
            user = authenticate(username=username, password=password)

            # Login Success
            if user is not None :

                # Remember me
                if is_remember_me:
                    request.session.set_expiry(86400)

                # Login session
                login(request, user)  # the user is now logged in

                response = redirect(reverse('blog-home'))
                return response
            else:
                messages.add_message(request, messages.ERROR, 'wrong password',"danger")

    # Contexts
    context = {
        'category_list' : category_list,
        'form_auth' : form_auth
    }
    return render(request, 'blog/views/signin.html',context)