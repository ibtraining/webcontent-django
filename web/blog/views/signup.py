from django.shortcuts import render
from blog.models.category import Category
from blog.models.article import Article
from django.core.paginator import Paginator
from blog.forms.signup import SignupForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse

# Create your views here.
def signupView(request):
    category_list = Category.objects.filter(is_enabled=True)
    
    form_signup = SignupForm()
    # Submit Signup
    if request.method == "POST":

        form_signup = SignupForm(request.POST)

        # Check validation 
        if form_signup.is_valid():

            form_signup.save()
            username = form_signup.cleaned_data.get('username')
            raw_password = form_signup.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)

            # auto login
            login(request, user)
            response = redirect(reverse('blog-home'))
            return response

    # Contexts
    context = {
        'category_list' : category_list,
        'form_signup' : form_signup
    }
    return render(request, 'blog/views/signup.html',context)