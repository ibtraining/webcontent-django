from django.shortcuts import render
from blog.models.category import Category
from blog.models.article import Article
from django.core.paginator import Paginator

# Create your views here.
def homeView(request):
    category_list = Category.objects.filter(is_enabled=True)
    article_list = Article.objects.order_by('-created_datetime').filter(is_enabled=True)

    # pagination
    paginator = Paginator(article_list, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    # Contexts
    context = {
        'category_list' : category_list,
        'article_list' : article_list,
        'page_obj' : page_obj
    }
    return render(request, 'blog/views/home.html',context)