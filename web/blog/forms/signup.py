from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

class SignupForm(UserCreationForm):
    
    password1 = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','type': 'password'}))
    password2 = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','type': 'password'}))

    class Meta:
        model = User
        fields = ['username',  'password1', 'password2',]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Customer class css
        self.fields['username'].widget.attrs.update({'class': 'form-control'})