from django import forms


class SigninForm(forms.Form):
    
    username = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','type': 'password'}))
    is_remember_me = forms.BooleanField(required=False)