from django.contrib import admin
from blog.models.article import Article
from blog.models.category import Category
from blog.models.comment import Comment
# Register your models here.

class ArticleAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'category',
        'created_datetime',
        'updated_datetime',
        'is_enabled',
    )
    list_editable = (
        'is_enabled',
    )
    list_filter = (
        'category',
        'is_enabled'
    )
    search_fields = (
        'title',
    )
admin.site.register(Article,ArticleAdmin)

class CommentAdmin(admin.ModelAdmin):
    list_display = (
        'article',
        'created_datetime',
        'is_enabled',
    )
    list_editable = (
        'is_enabled',
    )
    list_filter = (
        'article',
        'is_enabled'
    )
admin.site.register(Comment,CommentAdmin)

class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'is_enabled',
    )
    list_editable = (
        'is_enabled',
    )
    list_filter = (
        'name',
        'is_enabled'
    )
admin.site.register(Category,CategoryAdmin)