from django.urls import path, re_path
from blog.views.home import homeView
from blog.views.article.list import articleListView
from blog.views.article.detail import articleDetailView
from blog.views.signin import signinView
from blog.views.signup import signupView
from blog.views.logout import logoutView


urlpatterns = [
    path('', homeView,name='blog-home'),
    path('article/', articleListView,name='blog-article-list'),
    path('article/<int:pk>/', articleDetailView,name='blog-article-detail'),
    path('signin/', signinView,name='blog-signin'),
    path('signup/', signupView,name='blog-signup'),
    path('logout/', logoutView,name='blog-logout'),
]