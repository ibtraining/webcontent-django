from datetime import datetime,date

class Person():

    name = None
    sex = None
    birth = None

    def __init__(self, name,sex,birth):
        self.name = name
        self.sex = sex
        self.birth = birth

    def get_age(self):

        born = datetime.strptime(self.birth, '%Y-%m-%d')
        today = date.today()
        age = today.year - born.year - ((today.month, today.day) < (born.month, born.day))
        return age
        

class Student(Person):
    def __init__(self, *args):
        super(Student, self).__init__(*args)
    
    def get_age(self):
        age = super(Student, self).get_age()
        return "%s Year" % age
        

student = Student("Jo","M","2000-01-01")
print('name',student.name)
print('sex',student.sex)
print('age',student.get_age())